<?php

use App\Imports\CSVImport;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class CSVSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new CSVImport, 'database/seeds/data.csv');
    }
}
