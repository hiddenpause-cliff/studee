<?php

namespace App\Imports;

use App\Segment;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CSVImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $row
    */
    public function collection(Collection $collection)
    {
        return $collection->each(function ($row) {
            $segment = Segment::firstOrCreate(
                ['external_id' => $row['segment']],
                ['name' => $row['segment_name']]
            );

            $family = $segment->families()->firstOrCreate(
                ['external_id' => $row['family']],
                ['name' => $row['family_name']]
            );

            $class = $family->animalClasses()->firstOrCreate(
                ['external_id' => $row['class']],
                ['name' => $row['class_name']]
            );

            $class->commodities()->Create(
                [
                    'external_id' => $row['commodity'],
                    'name' => $row['commodity_name']
                ]
            );
            return $segment;
        });
    }
}
