<?php

namespace App\Http\Controllers;

use App\AnimalClass;
use App\Commodity;
use App\Http\Requests\CreateCommodityRequest;
use App\Http\Requests\UpdateCommodityRequest;
use Illuminate\Http\Request;

class CommodityController extends Controller
{
    /**
     * Get a single Commodity
     *
     * @param Commodity $commodity A Commodity
     *
     * @return Iterable
     */
    public function get(Commodity $commodity)
    {
        $commodity->load(['animalClass']);

        return [
            'message' => '',
            'data' => $commodity,
            'status' => 200,
        ];
    }

    /**
     * Get all Families
     *
     * @param Request $request request options
     *
     * @return Iterable
     */
    public function getAll(Request $request)
    {
        $limit = $request->input('limit', 100);
        $offset = $request->input('offset', 0);

        $commodities = Commodity::take($limit)->skip($offset)->get();

        return [
            'message' => "{$commodities->count()} families returned",
            'data' => $commodities,
            'status' => 200,
        ];
    }

    /**
     * Create a Commodity
     *
     * @param Request $request request options
     *
     * @return Iterable
     */
    public function create(CreateCommodityRequest $request)
    {
        $commodity = new Commodity();
        $commodity->name = $request->input('name');

        $animalClass = AnimalClass::findOrFail($request->input('animal_class_id'));

        $commodity->animalClass()->associate($animalClass);

        $commodity->save();

        return [
            'message' => 'Commodity created',
            'data' => $commodity,
            'status' => 200,
        ];
    }

    /**
     * Update a single Commodity
     *
     * @param Commodity $commodity A Commodity
     *
     * @return Iterable
     */
    public function update(UpdateCommodityRequest $request, Commodity $commodity)
    {
        $classId = $request->input('animal_class_id', $commodity->animal_class_id);
        $commodity->update([
            'name' => $request->input('name')
        ]);

        if ($classId !== $commodity->animal_class_id) {
            $commodity->animalClass()->associate(AnimalClass::findOrFail($classId));
            $commodity->save();
        }

        return [
            'message' => 'Commodity updated',
            'data' => $commodity->fresh(),
            'status' => 200,
        ];
    }

    /**
     * Delete a single Commodity
     *
     * @param Commodity $commodity A Commodity
     *
     * @return Iterable
     */
    public function delete(Commodity $commodity)
    {
        $commodity->delete();

        return [
            'message' => "Commodity with ID {$commodity->id} deleted",
            'data' => null,
            'status' => 200,
        ];
    }

}
