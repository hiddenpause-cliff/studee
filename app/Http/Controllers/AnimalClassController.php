<?php

namespace App\Http\Controllers;

use App\AnimalClass;
use App\Family;
use App\Http\Requests\CreateAnimalClassRequest;
use App\Http\Requests\UpdateAnimalClassRequest;
use Illuminate\Http\Request;

class AnimalClassController extends Controller
{
    /**
     * Get a single AnimalClass
     *
     * @param AnimalClass $animalClass A AnimalClass
     *
     * @return Iterable
     */
    public function get(AnimalClass $animalClass)
    {
        $animalClass->load(['family']);
        return [
            'message' => '',
            'data' => $animalClass,
            'status' => 200,
        ];
    }

    /**
     * Get all AnimalClasses
     *
     * @param Request $request request options
     *
     * @return Iterable
     */
    public function getAll(Request $request)
    {
        $limit = $request->input('limit', 100);
        $offset = $request->input('offset', 0);
        $animalClasses = AnimalClass::take($limit)->skip($offset)->get();

        return [
            'message' => "{$animalClasses->count()} classes returned",
            'data' => $animalClasses,
            'status' => 200,
        ];
    }

    /**
     * Create an AnimalClass
     *
     * @param Request $request request options
     *
     * @return Iterable
     */
    public function create(CreateAnimalClassRequest $request)
    {
        $animalClass = new AnimalClass();
        $animalClass->name = $request->input('name');

        $family = Family::findOrFail($request->input('family_id'));

        $animalClass->family()->associate($family);

        $animalClass->save();

        return [
            'message' => 'AnimalClass created',
            'data' => $animalClass,
            'status' => 200,
        ];
    }

    /**
     * Update a single AnimalClass
     *
     * @param AnimalClass $animalClass An AnimalClass
     *
     * @return Iterable
     */
    public function update(UpdateAnimalClassRequest $request, AnimalClass $animalClass)
    {
        $familyId = $request->input('family_id', $animalClass->family_id);

        $animalClass->update([
            'name' => $request->input('name'),
        ]);

        if ($familyId !== $animalClass->family_id) {
            $animalClass->family()->associate(Family::findOrFail($familyId));
            $animalClass->save();
        }

        return [
            'message' => 'AnimalClass updated',
            'data' => $animalClass->fresh(),
            'status' => 200,
        ];
    }

    /**
     * Delete a single AnimalClass
     *
     * @param AnimalClass $animalClass A AnimalClass
     *
     * @return Iterable
     */
    public function delete(AnimalClass $animalClass)
    {
        $animalClass->delete();

        return [
            'message' => "AnimalClass with ID {$animalClass->id} deleted",
            'data' => null,
            'status' => 200,
        ];
    }

}
