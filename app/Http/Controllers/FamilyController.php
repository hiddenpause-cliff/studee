<?php

namespace App\Http\Controllers;

use App\Family;
use App\Http\Requests\CreateFamilyRequest;
use App\Http\Requests\UpdateFamilyRequest;
use App\Segment;
use Illuminate\Http\Request;

class FamilyController extends Controller
{
    /**
     * Get a single Family
     *
     * @param Family $family A Family
     *
     * @return Iterable
     */
    public function get(Family $family)
    {
        $family->load(['segment']);
        return [
            'message' => '',
            'data' => $family,
            'status' => 200,
        ];
    }

    /**
     * Get all Families
     *
     * @param Request $request request options
     *
     * @return Iterable
     */
    public function getAll(Request $request)
    {
        $limit = $request->input('limit', 100);
        $offset = $request->input('offset', 0);
        $families = Family::take($limit)->skip($offset)->get();

        return [
            'message' => "{$families->count()} families returned",
            'data' => $families,
            'status' => 200,
        ];
    }

    /**
     * Create a Family
     *
     * @param Request $request request options
     *
     * @return Iterable
     */
    public function create(CreateFamilyRequest $request)
    {
        $family = new Family();
        $family->name = $request->input('name');

        $segment = Segment::findOrFail($request->input('segment_id'));

        $family->segment()->associate($segment);

        $family->save();

        return [
            'message' => 'Family created',
            'data' => $family,
            'status' => 200,
        ];
    }

    /**
     * Update a single Family
     *
     * @param Family $family A Family
     *
     * @return Iterable
     */
    public function update(UpdateFamilyRequest $request, Family $family)
    {
        $segmentId = $request->input('segment_id', $family->segment_id);

        $family->update([
            'name' => $request->input('name'),
        ]);

        if ($segmentId !== $family->segment_id) {
            $family->segment()->associate(Segment::findOrFail($segmentId));
            $family->save();
        }

        return [
            'message' => 'Family updated',
            'data' => $family->fresh(),
            'status' => 200,
        ];
    }

    /**
     * Delete a single Family
     *
     * @param Family $family A Family
     *
     * @return Iterable
     */
    public function delete(Family $family)
    {
        $family->delete();

        return [
            'message' => "Family with ID {$family->id} deleted",
            'data' => null,
            'status' => 200,
        ];
    }

}
