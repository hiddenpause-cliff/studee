<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUpdateSegmentRequest;
use App\Segment;
use Illuminate\Http\Request;

class SegmentController extends Controller
{
    /**
     * Get a single Segment
     *
     * @param Segment $segment A Segment
     *
     * @return Iterable
     */
    public function get(Segment $segment)
    {
        return [
            'message' => '',
            'data' => $segment,
            'status' => 200,
        ];
    }

    /**
     * Get all Segments
     *
     * @param Request $request request options
     *
     * @return Iterable
     */
    public function getAll(Request $request)
    {
        $limit = $request->input('limit', 100);
        $offset = $request->input('offset', 0);
        $segments = Segment::take($limit)->skip($offset)->get();

        return [
            'message' => "{$segments->count()} segments returned",
            'data' => $segments,
            'status' => 200,
        ];
    }

    /**
     * Create a Segment
     *
     * @param Request $request request options
     *
     * @return Iterable
     */
    public function create(CreateUpdateSegmentRequest $request)
    {
        $segment = Segment::create([
            'name' => $request->input('name'),
        ]);

        return [
            'message' => 'Segment created',
            'data' => $segment,
            'status' => 200,
        ];
    }

    /**
     * Update a single Segment
     *
     * @param Segment $segment A Segment
     *
     * @return Iterable
     */
    public function update(CreateUpdateSegmentRequest $request, Segment $segment)
    {
        $segment->update([
            'name' => $request->input('name'),
        ]);

        return [
            'message' => 'Segment updated',
            'data' => $segment->fresh(),
            'status' => 200,
        ];
    }

    /**
     * Delete a single Segment
     *
     * @param Segment $segment A Segment
     *
     * @return Iterable
     */
    public function delete(Segment $segment)
    {
        $segment->delete();

        return [
            'message' => "Segment with ID {$segment->id} deleted",
            'data' => null,
            'status' => 200,
        ];
    }

}
