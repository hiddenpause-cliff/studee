<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\ViewErrorBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $exception)
    {
        $message = $this->getMessageWithFallback($exception);
        $status = !empty($exception->status) ? $exception->status : 500;

        if ($exception instanceof ModelNotFoundException) {
            $status = 404;
        } else if ($exception instanceof NotFoundHttpException) {
            $status = 404;
        }

        return response()->json(['status' => $status, 'message' => $message], $status);
    }

    private function getMessageWithFallback(Exception $exception)
    {
        return !empty($exception->getMessage())
            ? $exception->getMessage()
            : 'There was an error completing your request';
    }
}
