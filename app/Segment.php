<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Segment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'external_id',
    ];

    protected $hidden = [
        'external_id',
    ];

    public function families()
    {
        return $this->hasMany('App\Family');
    }
}
