<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnimalClass extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'external_id',
    ];

    protected $hidden = [
        'external_id',
    ];

    public function commodities()
    {
        return $this->hasMany('App\Commodity');
    }

    public function family()
    {
        return $this->belongsTo('App\Family');
    }
}
