<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'external_id',
    ];

    protected $hidden = [
        'external_id',
    ];

    public function animalClasses()
    {
        return $this->hasMany('App\AnimalClass');
    }

    public function segment()
    {
        return $this->belongsTo('App\Segment');
    }
}
