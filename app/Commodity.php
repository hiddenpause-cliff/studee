<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commodity extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'external_id',
    ];

    protected $hidden = [
        'external_id',
    ];

    public function animalClass()
    {
        return $this->belongsTo('App\AnimalClass');
    }
}
