# Studee code task

## Framework

Laravel was used to carry out this task, mainly due to my familiarity to it and it's ease of use for this kind of CRUD style API.

## Getting started

- Run `composer install`
- Run `docker-compose up` to get started and on completion:
    - `docker-compose exec app php artisan migrate:fresh --seed`
- The api will now run on 127.0.0.1/api. Refer to [schema](./swagger.json) for available endpoints

## Assumptions

- I have assumed that the CSV data is just to provide data to test out the API and so it is added during the Seed task and I wouldn't anticipate using it in a production environment. If so I would change how I go about the import
- I have assumed that cascading deletes are acceptable as it simplifies the delete functionality used throughout the CRUD operations
- Simple api token based authentication could be added with a small amount of effort, but it didn't seem essential for the task

## Other

I made a naming mistake with the Class type. By the time I realised that not all in the Class type weren't animals I'd named a fair amount of the API so I have stuck with it for the purpose of the task.
