<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Illuminate\Http\Exceptions\HttpResponseException;

Route::group(['prefix' => 'segment'], function () {
    Route::model('segment', '\App\Segment');

    Route::get('/{segment}', 'SegmentController@get');
    Route::get('/', 'SegmentController@getAll');
    Route::post('/', 'SegmentController@create');
    Route::put('/{segment}', 'SegmentController@update');
    Route::delete('/{segment}', 'SegmentController@delete');
});

Route::group(['prefix' => 'family'], function () {
    Route::model('family', '\App\Family');

    Route::get('/{family}', 'FamilyController@get');
    Route::get('/', 'FamilyController@getAll');
    Route::post('/', 'FamilyController@create');
    Route::put('/{family}', 'FamilyController@update');
    Route::delete('/{family}', 'FamilyController@delete');
});

Route::group(['prefix' => 'animal-class'], function () {
    Route::model('animal_class', '\App\AnimalClass');

    Route::get('/{animal_class}', 'AnimalClassController@get');
    Route::get('/', 'AnimalClassController@getAll');
    Route::post('/', 'AnimalClassController@create');
    Route::put('/{animal_class}', 'AnimalClassController@update');
    Route::delete('/{animal_class}', 'AnimalClassController@delete');
});

Route::group(['prefix' => 'commodity'], function () {
    Route::model('commodity', '\App\Commodity');

    Route::get('/{commodity}', 'CommodityController@get');
    Route::get('/', 'CommodityController@getAll');
    Route::post('/', 'CommodityController@create');
    Route::put('/{commodity}', 'CommodityController@update');
    Route::delete('/{commodity}', 'CommodityController@delete');
});

Route::fallback(function () {
    throw new HttpResponseException(response()->json(['status' => 404, 'messages' => 'Route not defined']));

});
